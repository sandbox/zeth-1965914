jQuery(function() {
	jQuery("a[href$='logout']").live("click", function(e) {
		e.preventDefault();
		var base = jQuery("#polaris_ils_logout_frame").data('baseurl');
		jQuery("#polaris_ils_logout_frame").attr("src", "http://" + base + "/logon.aspx?logoff=1&src=default.aspx");
		var url = jQuery(this).attr("href");
		//window.location = url;
		
		if (url) {
			setTimeout(function() {window.location = url;}, 500);
		}
		
	})
	
})
